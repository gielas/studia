package mpr;

public class Permission {
	
	private int permissionLevel;

	public int getPermissionLevel() {
		return permissionLevel;
	}
	public void setPermissionLevel(int permissionLevel) {
		this.permissionLevel = permissionLevel;
	}

}
